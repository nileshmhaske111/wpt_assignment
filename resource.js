const express = require('express');
const app = express();
const mysql = require('mysql2');
const connectdb = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'cdac',
    database: 'cdacdb',
    portno: 3306
});

app.use(express.static('staticfolder'));

app.get('/insert', (request,responce) => {
    connectdb.query('insert into resource values (?,?,?)', [request.query.resno, request.query.resname, request.query.resstats], (error, result) => {
        if (error) {
            console.log("Insertion Failed");
        } else {
            if (result.affectedRows == 0) {
                responce.send(false);

            } else {
                console.log("Details inserted");
                responce.send(true);
            }
        }
    });
});

app.get('/update', (request,responce) => {
    console.log("Update Failed");
    connectdb.query('update resource set name=?, status=? where no=?', [request.query.resname, request.query.resstats, request.query.resno], (error, result) => {
        if (error) {
        } else {
            if (result.affectedRows == 0) {
                responce.send(false);
            } else {
                console.log("Details updated");
                responce.send(true);
            }
        }
    });
});

app.get('/select', (request,responce) => {
    connectdb.query('select * from resource where itemno = ?', [request.query.resno], (err, res1) => {
        if (error) {
            console.log("Failed");
        } else {
            if (result.length > 0) {
                responce.send(true);
            } else {
                responce.send(false);
            }
        }
    });
});

app.listen(3000, () => {
    console.log("Server listening on port 3000...");
});