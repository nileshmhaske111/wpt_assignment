const express = require('express');
const app = express();
const mysql = require('mysql2');
const connectdb = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'cdac',
    database: 'cdacdb',
    portno: 3306
});

app.use(express.static('staticfolder'));

app.get('/insert', (request, responce) => {
    connectdb.query('insert into item values (?,?,?,?)', [request.query.itemno, request.query.itemname, request.query.itemprice, request.query.itemcategory], (error, result) => {
        if (error) {
            console.log("Insertion Failed");
        } else {
            if (result.affectedRows == 0) {
                responce.send(false);

            } else {
                console.log("details inserted");
                responce.send(true);
            }
        }
    });
});

app.get('/update', (request, responce) => {
    console.log("Update Failed");
    connectdb.query('update item set itemname=?, price=?, category=? where itemno=?', [request.query.itemname, request.query.itemprice, request.query.itemcategory, request.query.itemno], (error, result) => {
        if (error) {
        } else {
            if (result.affectedRows == 0) {
                responce.send(false);
            } else {
                console.log("details updated");
                responce.send(true);
            }
        }
    });
});

app.get('/select', (request, responce) => {
    connectdb.query('select * from item where itemno = ?', [request.query.itemno], (error, result) => {
        if (error) {
            console.log("Failed");
        } else {
            if (result.length > 0) {
                console.log("Working");
                responce.send(true);
            } else {
                responce.send(false);
            }
        }
    });
});

app.listen(3000, () => {
    console.log("Server listening on port 3000...");
});