const express = require('express');
const app = express();
const mysql = require('mysql2');
const contact = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'cdac',
    database: 'cdacdb',
    portno: 3306
});

app.use(express.static('staticfolder'));

app.get('/getdetails', (request, responce) => {
    contact.query('select * from account where accno = ?', [request.query.accountno], (error, result) => {
        if (error) {
            console.log("Error");
        } else {
            if (result.length > 0) {
                responce.send(result[0]);
            } else {
                responce.send(true);
            }
        }
    });
});

app.listen(3000, () => {
    console.log("Server listening on port 3000...");
});